$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// main of html

$(document).ready(() => {
    $('.nav-toggle').click(function (e) {
        e.preventDefault();
        $("html").toggleClass("openNav");
        $(".nav-toggle").toggleClass("active");
        $('main').css('margin-left', '300px');
        $('.range-price').show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    });

    // Product

    // product deleting

    $('.js_delete_product').on('click', function () {
        let id = $(this).data('id');
        let self = $(this);
        $(this).parents('tr').remove();
        $.ajax({
            method: 'delete',
            url: `products/delete/${id}`,
            success: function (result) {
                if (result) {
                    self.parents('tr').remove();
                }
            },
            error(e) {

            }
        });
    })
});

// Edit

$(document).on('click', '.js_edit_product', function () {
    let id = $(this).data('id');
    $.ajax({
        method: 'get',
        url: `products/edit/${id}`,
        success: function (result) {
            $('#product_edit_modal input[name="product_name"]').val(result.product.name);
            $('#product_edit_modal input[name="product_price"]').val(result.product.price);
            $('#product_edit_modal input[name="updated_at"]').val(result.product.updated_at);
            $('#product_edit_modal input[name="id"]').val(id);
            $('#product_edit_modal').modal('show');
        },
        error(e) {

        }
    })
});

// Update

$('.js_update_product').on('click', function () {
    let newData = new Object();
    let data = $('#js_updated_data').serializeArray();
    jQuery.each(data, function () {
        newData[this.name] = this.value;
    });
    $.ajax({
        method: 'put',
        url: `products/update/${newData.id}`,
        type: 'JSON',
        data: data,
        success: function (result) {
            if (result) {
                let root_path = $(`.product_list tr[data-id=${result.productObject.id}]`);
                root_path.children(`td[data-target='name']`).html(result.productObject.name);
                root_path.children(`td[data-target='price']`).html(result.productObject.price);
                root_path.children(`td[data-target='price']`).html(result.productObject.price);
                swal({
                    text: `${result.message}`,
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true,
                    icon: "success",
                }).then(() => {
                    $('#product_edit_modal').modal('hide');
                })
            }
        },
        error(e) {

        }
    });
});
// live search
$(document).on('keydown', '.js_live_search', function (e) {
    e = e || window.event;
    if (e.keyCode == 13) {
        e.preventDefault()
    }
});

$('.js_live_search').on('keyup', function () {
    let searches = $(this).val();
    $.ajax({
        method: 'get',
        url: `/search`,
        data: {
            'str': searches
        },
        success: function (result) {
            $('#search_result').html(result.view);
        },
        error(e) {
            console.log(e);
        }
    });
});
$(function () {
    let rangeValues = {
        from: 0,
        to: 5000
    };
    $("#range").ionRangeSlider({
        hide_min_max: true,
        keyboard: true,
        min: 0,
        max: 5000,
        from: 0,
        to: 5000,
        type: 'double',
        step: 1,
        prefix: "$",
        grid: true,
        onFinish: function (data) {
            rangeValues.from = data.from;
            rangeValues.to = data.to;

            let params = {};
            let sort = getUrlParameter('sort');
            let page = getUrlParameter('page');

            if (typeof sort !== undefined) {
                params.sort = sort;
            }
            if (typeof page !== undefined) {
                params.page = page;
            }
            params.range = `${rangeValues.from}%3B${rangeValues.to}`;
            var str = jQuery.param(params);
            window.history.pushState('', "", str);
            $.ajax({
                method: 'get',
                url: `/products/filter`,
                data: rangeValues,
                success: function (response) {
                    if (response) {
                        $('#result_table').html(response.view);
                    }

                },
                error(e) {
                    console.log(e);
                }
            });
        },
    });
});

// url generator

let getUrlParameter = function getUrlParameter(sParam) {
    let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

// Image resizing

$(".product_image").on('click', function () {
    let image_src = $(this).attr('src');
    $('#preview_img').attr('src', image_src);
    $('#imagePreviewModal').modal('show');

});

// Storing

$('#product_form').on('submit', function (e) {
    e.preventDefault();
    let formData = new FormData($(this)[0]);
    $.ajax({
        method: 'post',
        url: '/products',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            $(`#result_table`).append(response.view);
            $('.errors').empty();
            swal({
                text: `${response.message}`,
                value: true,
                visible: true,
                className: "",
                closeModal: true,
                icon: "success",
            }).then(() => {
                if (response.success) {
                    window.location.href = 'http://127.0.0.1:8000/'
                }
            });

        }
    })
});


// Category

// saving
function saveCategory() {
    let newData = {};
    let data = $('#category_form').serializeArray();
    jQuery.each(data, function () {
        newData[this.name] = this.value;
    });
    $.ajax({
        method: 'post',
        url: '/categories',
        data: newData,
        success: function (result) {
            if (result.errors) {
                $('.errors').text(result.errors);
            }
            else {
                $(`#category_list`).append(`
                        <tr>
                            ${renderCategoryTableTD(result.category)}
                        </tr>
                    `);
                $('.errors').empty();
            }
        }
    })
}

$('#category_form').submit((e) => {
    e.preventDefault();
    saveCategory()
});
$('.js_category_save').on('click', function (e) {
    saveCategory()
});

function renderCategoryTableTD(category) {
    return (`
            <td>${category.id}</td>
            <td>${category.name}</td>
            <td>${category.created_at}</td>
            <td><button data-id="${category.id}" class="btn btn-secondary js_edit_category">Edit</button></td>
            <td><button data-id="${category.id}" class="btn btn-secondary js_delete_category">Delete</button></td>
        `)
}

// Editing

$(document).on('click', '.js_edit_category', function () {
    let id = $(this).data('id');
    $.ajax({
        method: 'get',
        url: `/categories/${id}/edit`,
        success: function (result) {
            $('#category_edit_modal input[name ="name"]').val(result.result.name);
            $('#category_edit_modal input[name ="created_at"]').val(result.result.created_at);
            $('#category_edit_modal input[name ="id"]').val(id);
            $('#category_edit_modal').modal('show')
        }
    })
});


// Deleting

$('.js_delete_category').on('click', function () {
    let id = $(this).data('id');
    let _this = $(this);
    $.ajax({
        method: 'delete',
        url: `/categories/${id}`,

        success: function (result) {

            if (result) {
                _this.parents('tr').remove()
            }
        },
        error(e) {
            console.log(e);
        }
    })
});


// Update

$('.js_update_category').on('click', function () {
    let newData = {};
    let data = $('#js_updated_data').serializeArray();
    jQuery.each(data, function () {
        newData[this.name] = this.value;
    });
    $.ajax({
        method: 'put',
        url: `categories/${newData.id}`,
        type: 'JSON',
        data: data,
        success: function (response) {
            if (response) {
                $(`.category_list tr[data-id=${response.category.id}] td[data-target='name']`).html(response.category.name);
                $('#category_edit_modal').modal('hide');
            }
        },
        error(e) {

        }
    });
});

// Comments Part
//storing

$("#comment").keyup(function (e) {
    var key = e.which;
    if (key === 13) {
        $("#save_comment").click();
        $('#comment_form')[0].reset()
    }
});
$(document).on('click', '#save_comment', function (e) {
    e.preventDefault();
    let _data = {};
    let data = $('#comment_form').serializeArray();
    jQuery.each(data, function () {
        _data[this.name] = this.value
    });

    $.ajax({
        url: '/comments',
        method: 'post',
        data: _data,
        success: function (response) {
            $('.comments-container>ul').append(response.view)
        }
    })
});

//like/dislike

$(document).on('click', '.like', function (e) {
    let commentId = $(this).data('id');
    let icon = $(this).children('i');
    $.ajax({
        url: '/likes',
        method: 'post',
        data: {
            'id': commentId,
        },
        success: function (response) {
            if (response.action === 'like') {
                icon.css('color', 'red')
            }
            else {

                icon.css('color', 'grey')
            }
        }
    })
});

// Delete
$(document).on('click', '.delete', function () {
    let id = $(this).data('id');
    let _this = $(this);
    $.ajax({
        method: `delete`,
        url: `/comments/${id}`,
        success: function (response) {
            if (response) {
                _this.parents('.comment-list').remove();
            }
        },
        error(e) {
            console.log(e)
        }
    })
});
// Reply Part

//form rendering


$(document).on('click', '.open-reply-form', function (e) {
    let commentId = $(this).data('id');
    let url = replyStoreRoute;
    console.log($('.reply-box').children().length > 0);
    if ($('.reply-box').children().length > 0) {
        $('#reply_form').remove()
    } else {
        $(this).parents('.comment-box').children('.reply-box').append(replyForm(commentId, url));
        $('#reply_form').show('fast')
    }
});

function replyForm(comment_id, url) {
    let csrf_token = $('meta[name="csrf-token"]').attr('content');
    return (`
     <form action="${url}" method="post"
              id="reply_form" class="reply_form">
            <input type="hidden" name="_token" value="${csrf_token}">
            <label for="reply"></label>
            <textarea name="replies" id="reply-textarea" class="form-control"> </textarea>
            <br>
            <button class="btn btn-primary send-reply" type="button" value=${comment_id}>Reply
            </button>
        </form>
        `)
}

// Storing

$(document).on('keyup', "#reply-textarea", function (e) {
    e = e || window.event;
    if (e.keyCode === 13) {
        $('.send-reply').click();
        $('#reply_form')[0].reset()
    }
});

$(document).on('click', '.send-reply', function () {
    let data = {};
    let comment_id = $(this).val();
    $replyFormData = $('#reply_form').serializeArray();
    jQuery.each($replyFormData, function () {
        data[this.name] = this.value
    });
    data['comment_id'] = comment_id;
    let parent_li = $(this).parents('.comment-box');
    $.ajax({
        method: 'post',
        url: '/replies',
        data: data,
        success: function (response) {
            parent_li.children('ul').append(repliesList(response.li));
        },
        error: function (e) {
        }
    })
});

function repliesList(response) {
    return (`
<br>
     <li class="reply-item"> ${response}</li>
    `)
}

// Reply  delete
$(document).on('click', '.reply-delete', function () {
    let id = $(this).data('id');
    let li = $(this).parents('.reply-list')[0];
    $.ajax({
        method: `delete`,
        url: `/replies/${id}`,
        success: function (response) {
            console.log(response);
            li.remove();
        },
        error(e) {

        }
    })
});

// Reply like/dislike

$(document).on('click', '.reply-like', function () {
    let id = $(this).data('id');
    let icon = $(this).children('i');
    $.ajax({
        method: `post`,
        url: `/replies/${id}`,
        success: function (response) {
            if (response.action === 'like') {
                icon.css('color', 'red')
            }
            else {
                icon.css('color', 'grey')
            }
        },
        error(e) {

        }
    })
});
$(document).on('click', '.reset_button', function () {
    let my_range = $("#range").data("ionRangeSlider");
    my_range.reset();
});

