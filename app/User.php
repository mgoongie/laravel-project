<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use PhpParser\Builder\Class_;

class User extends Authenticatable
{
    use Notifiable;
    const ADMIN_TYPE = 'admin';
    const DEAFULT_TYPE = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function comments()
    {

        return $this->hasMany(Comment::class, 'user_id');

    }

    public function likes()
    {
        return $this->hasMany(Like::class, 'user_id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
