<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reply extends Model
{
    use SoftDeletes;
    protected $fillable = ['body', 'user_id'];

    public function repliable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);

    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }
}
