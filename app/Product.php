<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'price', 'category_id'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->withDefault([
            'name' => 'uncategorized'
        ]);
    }

    public function files()
    {
        return $this->hasMany(File::class, 'product_id');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->withCount('likes');
    }
}

