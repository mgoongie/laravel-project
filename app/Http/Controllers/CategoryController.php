<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('category', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//examples//
//        $category = new Category();
//        $category->name = $request->input('name');
//        $categoryObject = $category->save();

//        $success = Category::query()->insert([
//            'name' => $request->input('name'),
//        ]);
        //        $category = Category::query()->create([
//            'name' => $request->input(category'name')
//        ]);
        $category = Validator::make($request->all(), [
            'name' => 'required|unique:categories|max:255'
        ]);
        if ($category->fails()) {
            return response()->json([
                'errors' => $category->errors()->all()
            ]);
        }
        $row = Category::query()->create([
            'name' => $request->input('name'),
        ]);

        return response()->json([
            'category' => $row
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::query()->find($id);
        return response()->json([
            'success' => true,
            'result' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::query()->find($id);
        $category->update([
            'name' => $request->input('name'),
        ]);
        return response()->json([
            'success' => true,
            'category' => $category,
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::query()->find($id);
        $category->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
