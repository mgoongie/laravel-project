<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Like;
use App\Product;
use App\Reply;
use function Couchbase\defaultDecoder;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $comment;

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->comment = '\App\Comment';
        $comment = $this->comment::query()->find($request->id);
        $isLiked = $comment->likes->where('user_id', auth()->user()->id)->first();

        if (!$isLiked) {
            $action = 'like';
            $like = $comment->likes()->save(new Like([
                'likes' => 1,
                'user_id' => Auth::user()->id
            ]));
        } else {
            $action = 'dislike';
            $isLiked->delete();
        }

        return response()->json([
            'success' => true,
            'action' => $action
        ]);
    }

    public function replyLikes($id)
    {
       $reply = Reply::query()->find($id);
        $isLiked = $reply->likes->where('user_id', auth()->user()->id)->first();

        if (!$isLiked) {
            $action = 'like';
            $like = $reply->likes()->save(new Like([
                'likes' => 1,
                'user_id' => Auth::user()->id
            ]));
        } else {
            $action = 'dislike';
            $isLiked->delete();
        }

        return response()->json([
            'success' => true,
            'action' => $action
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
