<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{


    public function admin()
    {
        return view('admin');
    }

    public function not_admin()
    {
        return view('notadmin');
    }
}
