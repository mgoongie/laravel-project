<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $comments  = Comment::query()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'body' => 'required'
        ]);
        $product = Product::query()->find($request->input('product'));
        $comment = $product->comments()->save(new Comment([
            'user_id' => Auth::user()->id,
            'body' => $request->comment,
        ]));
        $time = Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans();
        $view = View::make('comments')->with([
            'comment' => $comment,
            'time' => $time
        ])->render();

        return response()->json([
            'view' => $view,
            'success'=> true
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::query()->find($id);
        $comment->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
