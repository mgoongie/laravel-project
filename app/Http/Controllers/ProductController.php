<?php

namespace App\Http\Controllers;

use App\Category;
use App\File;
use App\Like;
use Illuminate\Support\Carbon;
use App\Product;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;


class ProductController extends Controller
{
    public function index(Request $request)
    {

        $products = Product::query()->with(['category:id,name', 'files']);
        if ($request->has('sort')) {
            $products->orderBy('price', Input::get('sort'));
        }

        $products = $products->paginate(10)->appends([
            'sort' => $request->query('sort'),
            'range' => $request->query('range'),
        ]);

        return view('home', [
            'products' => $products,
        ]);
    }

    public function create()
    {
        $categories = Category::query()->get();
        return view('product', compact('categories'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'name' => 'required|max:255',
                'price' => 'required|max:1000000',
                'files' => 'required|mimes:jpeg,jpg,gif,png|max:1000000'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ]);
        }

        $category = Category::query()->find($request->input('category'));
        $product = $category->products()->save(new Product([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
        ]));


        $view = View::make('product_row')->with([
            'product' => $product
        ])->render();
        $this->upload($request->file('files'), $product);

        return response()->json([
            'success' => true,
            'message' => 'item has been added',
            'view' => $view,
        ]);

    }

    public function edit(Product $product)
    {

        return response()->json([
            'success' => true,
            'product' => $product,
        ]);
    }

    public function update(Request $request, Product $product)
    {
        $product->update([
            'name' => $request->input('product_name'),
            'price' => $request->input('product_price')
        ]);
        return response()->json([
            'success' => true,
            'message' => 'information has been updated',
            'productObject' => $product
        ]);
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json([
            'success' => true,
        ]);
    }

    public function upload($file, $product)
    {
        $file->move(public_path('image'), $file->getClientOriginalName());
        return $product->files()->save(new File([
            'name' => 'image/' . $file->getClientOriginalName(),
        ]));

    }

    public function search(Request $request)
    {
        $results = Product::query()->where('name', 'LIKE', '%' . $request->input('str') . '%')->limit(5)->get();
        $view = View::make('search')->with([
            'results' => $results
        ])->render();
        return response()->json([
            'success' => true,
            'view' => $view
        ]);
    }

    public function filter(Request $request)
    {
        $min = $request->query('from');
        $max = $request->query('to');
        $results = Product::query()
            ->whereBetween('price', [$min, $max])
            ->limit(10)
            ->get();

        $view = View::make('range-results')->with([
            'results' => $results
        ])->render();
        return response()->json([
            'success' => true,
            'view' => $view
        ]);
    }

    public function show(Product $product)
    {
        if(!Auth::check()) {
            $product = Product::query()->find($product->id);
        }
        else {
            $product = Product::query()->where('id', $product->id)->with(['comments', 'comments.likes' => function ($query) {
                $query->where('user_id', auth()->user()->id);
            }])->first();
        }
        return view('singlePage', [
            'product' => $product,
        ]);
    }

}
