<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User as UserModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class User extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //My option
//        $name = $this->ask('Enter your name?');
//        $email = $this->ask('Enter your email?');
//        $password = $this->secret('Enter your password');
//        $this->confirm('Are you sure?');
//        $this->info('Fine');
//        $this->insert($name, $email, $password);

        //The short option

        $userObject = new UserModel([
            'name' => $this->ask('Enter your name?'),
            'email' => $this->ask('Enter your email?'),
            'password' => $this->secret('Enter your password'),
        ]);
        $this->insert($userObject);
        $this->info('Fine');
    }

    protected function insert(UserModel $userObject)
    {
        UserModel::query()->insert([
            'name' => $userObject->name,
            'email' => $userObject->email,
            'password' => Hash::make($userObject->password),
        ]);
    }
}
