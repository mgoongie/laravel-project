<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public $fillable = ['name'];

    public function product() {

        return $this->belongsTo(Product::class, 'product_id');
    }
}
