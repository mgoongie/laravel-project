<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'ProductController@index')->name('product.index');
Route::resource('/categories', 'CategoryController');
Route::get('/products/edit/{product}', 'ProductController@edit')->name('product.edit');
Route::get('/products', 'ProductController@create')->name('product.create');
Route::post('/products', 'ProductController@store')->name('product.store');
Route::put('/products/update/{product}', 'ProductController@update')->name('product.update');
Route::delete('/products/delete/{product}', 'ProductController@destroy')->name('product.destroy');
Route::get('/search', 'ProductController@search')->name('product.search');
Route::get('/products/filter', 'ProductController@filter')->name('product.filter');
Route::get('/products/{product}', 'ProductController@show')->name('product.show');
Route::resource('/comments', 'CommentController');
Route::resource('/likes', 'LikeController');
Route::post('/replies/{replies}', 'LikeController@replyLikes');
Route::resource('/replies', 'ReplyController');
//Admin part
Route::get('/admin', 'AdminController@admin')->middleware('admin')->name('admin');
Route::get('/notadmin', 'AdminController@not_admin');



