<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Database seeding started');
        \App\Category::truncate();
        \App\Product::truncate();
        \App\File::truncate();
        $this->command->info('Models have been truncated');

        $categories = factory(App\Category::class, 10)->create();
        $this->command->info('categories have been created');
        $categories->each(function ($category) {
            $products = $category->products()->saveMany(factory(App\Product::class, rand(20, 30))->make());
            $products->each(function ($product) {
                $product->files()->saveMany(factory(App\File::class, 5)->make());
            });
        });
    }
}
