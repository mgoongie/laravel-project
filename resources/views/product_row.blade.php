{{--@php--}}

        {{--dd($product->id)--}}
        {{--@endphp--}}
<tr data-id="{{$product->id}}">
    <td>{{$product->category->name}}</td>
    <td data-target="name">
        @if(count($product->files) != 0)
            <img src="{{asset($product->files[0]->name)}}" alt=""
                 style="height: 70px; width: 70px">
        @else
            <img src="https://cdn3.iconfinder.com/data/icons/faticons/32/picture-01-512.png"
                 alt="" style="height: 70px; width: 70px">
        @endif
        {{$product->name}}
    </td>
    <td data-target="price">{{$product->price}}</td>
    <td data-target="created_at">{{$product->created_at}}</td>
    <td>
        <button data-id="{{$product->id}}" type="button"
                class="btn btn-secondary js_edit_product">Edit
        </button>
    </td>
    <td>
        <button data-id="{{$product->id}}" type="button"
                class="btn btn-secondary js_delete_product">Delete
        </button>
    </td>
</tr>