
<li>
    <div class="comment-main-level">
        <!-- Avatar -->
        <div class="comment-avatar"><img
                    src="https://vignette.wikia.nocookie.net/csydes-test/images/6/6b/%28AnimeFreak%29_Mirai_Kuriyama.png/revision/latest?cb=20170404065736"
                    alt=""></div>
        <!-- Contenedor del Comentario -->
        <div class="comment-box">
            <div class="comment-head">
                <h6 class="comment-name by-author"><a href="http://creaticode.com/blog">{{$comment->user->name}}</a></h6>
                <button class="delete" data-id="{{$comment->id}}">
                    <i class="fa fa-heart"></i>
                </button>
                <button class="like" data-id="{{$comment->id}}">
                    <i class="fa fa-reply"></i>
                </button>
                <button class="delete" data-id="{{$comment->id}}">
                    <i class="fa fa-trash"> </i>
                </button>
                <span>{{$time}}</span>
            </div>
            <div class="comment-content">
                {{$comment->body}}
            </div>
            <div class="reply-box"></div>
            <ul class="comments-list reply-list"></ul>
        </div>

    </div>
</li>

