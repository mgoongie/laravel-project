@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 justify-content-center">
                <div class="card text-center">
                    <img class="card-img-top" src="{{$product->files->first() ? $product->files->first()->name : ''}}" alt="Card image cap">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$product->name}}</h5>
                        <p class="card-text">{{$product->price}}</p>
                    </div>
                </div>
                @if(Auth::check())
                    <form action="{{route('comments.store')}}" method="post" id="comment_form">
                        @csrf
                        <input type="hidden" name="product" value="{{$product->id}}">
                        <label for="comment"></label>
                        <textarea name="comment" id="comment" class="form-control"
                                  placeholder="your comment here..."> </textarea>
                        <br>
                        <button class="btn btn-primary" type="button" id="save_comment">Add comment</button>
                    </form>
                @endif
            </div>
            <div class="col-md-2" id="comments_section">
                <div class="comments-container">
                    <ul id="comments-list" class="comments-list">
                        @if(isset($product))
                            @foreach($product->comments as $comment)
                                <li class="comment-list">
                                    <div class="comment-main-level">
                                        <!-- Avatar -->
                                        <div class="comment-avatar"><img
                                                    src="https://vignette.wikia.nocookie.net/csydes-test/images/6/6b/%28AnimeFreak%29_Mirai_Kuriyama.png/revision/latest?cb=20170404065736"
                                                    alt=""></div>
                                        <!-- Contenedor del Comentario -->
                                        <div class="comment-box">
                                            <div class="comment-head">
                                                <h6 class="comment-name by-author">
                                                    <a>{{$comment->user->name}}</a>
                                                </h6>
                                                @if(Auth::check())
                                                    <button class="like"
                                                            value="{{$comment->commentable->id}}"
                                                            data-id="{{$comment->id}}">
                                                        <i style="color: {{count($comment->likes) ? 'red' : 'grey'}}"
                                                           class="fa fa-heart like_icon"></i>
                                                    </button>
                                                    <button class="open-reply-form" data-id="{{$comment->id}}">
                                                        <i class="fa fa-reply"> </i>
                                                    </button>
                                                    <button class="delete" data-id="{{$comment->id}}">
                                                        <i class="fa fa-trash"> </i>
                                                    </button>
                                                    <span>{{$comment->created_at->diffForHumans()}}</span>
                                                @endif
                                            </div>
                                            <div class="comment-content">
                                                {{$comment->body}}
                                            </div>
                                            <div class="reply-box"></div>
                                            <ul class="comments-list reply-list">
                                                @foreach($comment->replies as $reply)
                                                    <li class="reply-list">
                                                        <div class="comment-avatar"><img
                                                                    src="https://img.purch.com/w/660/aHR0cDovL3d3dy5saXZlc2NpZW5jZS5jb20vaW1hZ2VzL2kvMDAwLzAwNy80MjEvb3JpZ2luYWwvMDUwNzExX2dlbmVyaWNfY293XzAyLmpwZw=="
                                                                    alt=""></div>
                                                        <div class="comment-box">
                                                            <div class="comment-head">
                                                                <h6 class="comment-name"><a
                                                                            href="http://creaticode.com/blog"></a></h6>
                                                                @if(Auth::check())
                                                                    <button class="reply-like" data-id="{{$reply->id}}">
                                                                        <i class="fa fa-heart"></i>
                                                                    </button>
                                                                    <button class="reply-delete"
                                                                            data-id="{{$reply->id}}">
                                                                        <i class="fa fa-trash"> </i>
                                                                    </button>
                                                                    <span> {{$reply->created_at->diffForHumans()}}</span>
                                                                @endif
                                                            </div>
                                                            <div class="comment-content">
                                                                {{$reply->body}}
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
