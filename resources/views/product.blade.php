@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('product.store') }}" class="col-md-5 mx-auto mt-5" id="product_form" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
            <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>
            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name"   autofocus>

                    <span role="alert">
                        <strong class="errors_name"></strong>
                    </span>
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-md-3 col-form-label text-md-right">{{ __('Price') }}</label>
            <div class="col-md-6">
                <input id="price" type="number" class="form-control"
                       name="price">

                <span role="alert">
                        <strong class="errors_price"></strong>
                    </span>
            </div>
        </div>

        {{-- save the results--}}

        <div class="form-group row">
            <label for="category" class="col-md-3 col-form-label text-md-right">{{ __('Category') }}</label>
            <div class="col-md-6">
                <select name="category" id="category" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}"> {{$category->name}} </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="upload" class="col-md-3 col-form-label text-md-right">{{ __('Upload') }}</label>
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="files">
                        <span></span>
                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-3">
                <button type="submit" class="btn btn-primary js_product_save">
                    {{ __('Save') }}
                </button>
            </div>
        </div>
    </form>
@endsection