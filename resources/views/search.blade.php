<div>
    @foreach($results as $result)
        <div class="card-group">
            <div class="card">
                <img class="card-img-top" src="{{asset($result->files[0]->name)}}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{$result->name}}</h5>
                    <p class="card-text">Price : {{$result->price}} $</p>
                    <p class="card-text">
                        <small class="text-muted"></small>
                    </p>
                </div>
            </div>
        </div> <br>
    @endforeach
</div>