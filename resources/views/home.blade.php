@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table product_list" id="result_table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">category</th>
                                <th scope="col">name</th>
                                <th scope="col">image</th>
                                <th scope="col">price</th>
                                <th scope="col">created at</th>
                                @if(Auth::check())
                                <th scope="col">edit</th>
                                <th scope="col">delete</th>
                                @endif
                                <th scope="col">view</th>
                            </tr>
                            </thead>
                            @foreach($products as $product)
                                <tr data-id="{{$product->id}}">
                                    <td>{{$product->category->name}}</td>
                                    <td data-target="name">{{$product->name}}</td>
                                    <td data-target="image_file">
                                        @if(count($product->files) != 0)
                                            <a id="resize"><img src="{{asset($product->files[0]->name)}}" alt=""
                                                                style="height: 70px; width: 70px" class="product_image">
                                            </a>
                                        @else
                                            <a id="resize"> <img
                                                        src="https://cdn3.iconfinder.com/data/icons/faticons/32/picture-01-512.png"
                                                        alt="" style="height: 70px; width: 70px" class="product_image">
                                            </a>
                                        @endif
                                    </td>
                                    <td data-target="price">{{$product->price}}</td>
                                    <td data-target="created_at">{{$product->created_at}}</td>
                                    @if(Auth::check())
                                    <td>
                                        <button data-id="{{$product->id}}" type="button"
                                                class="btn btn-secondary js_edit_product">Edit
                                        </button>
                                    </td>
                                    <td>
                                        <button data-id="{{$product->id}}" type="button"
                                                class="btn btn-secondary js_delete_product">Delete
                                        </button>
                                    </td>
                                    @endif
                                    <td>

                                        {{--my option-- ^-^}}

                                        {{--<button data-id="{{$product->id}}" type="button"--}}
                                                {{--class="btn btn-secondary js_view_product" onclick="window.location=`{{ url("/products/{$product->id}/show")}}`"> View product--}}
                                        {{--</button>--}}

                                        {{--Mihran's option :P--}}
                                        <a href="{{route('product.show', ['product' => $product->id])}}" class="btn btn-secondary js_view_product" class="">View product</a>
                                    </td>
                                </tr>
                            @endforeach

                        </table>


                        {{$products->links()}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                {{--live search results component--}}

                <div class="col-md-12" id="search_result">

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="product_edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Make changes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="js_updated_data">
                        @csrf
                        <input type="hidden" name="id" value="">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="product_name" id="product_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="type" class="col-sm-2 col-form-label">updated_at</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="updated_at" id="product_type">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-sm-2 col-form-label">Price</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="product_price" id="price">
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary js_update_product">Update</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="imagePreviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body mbody">
                    <img src="" alt="" id="preview_img" width="700">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

