{{--reply level of comment--}}
<div class="comment-avatar"><img
            src="https://img.purch.com/w/660/aHR0cDovL3d3dy5saXZlc2NpZW5jZS5jb20vaW1hZ2VzL2kvMDAwLzAwNy80MjEvb3JpZ2luYWwvMDUwNzExX2dlbmVyaWNfY293XzAyLmpwZw=="
            alt=""></div>
    <div class="comment-box">
        <div class="comment-head">
            <h6 class="comment-name"><a href="http://creaticode.com/blog"></a></h6>
            <button class="reply-like" data-id="{{$reply->id}}">
                <i class="fa fa-heart"></i>
            </button>
            <button class="reply-of-reply" data-id="{{$reply->id}}">
                <i class="fa fa-reply"></i>
            </button>
            <button class="reply-delete" data-id="{{$reply->id}}">
                <i class="fa fa-trash"> </i>
            </button>
        </div>
        <div class="comment-content">
            {{$reply->body}}
        </div>
    </div>