<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">



</head>
<body>
<div class="primary-nav">
    <button href="#" class="hamburger open-panel nav-toggle" id="nav-toggle">
        <span class="screen-reader-text">Menu</span>
    </button>
    <nav role="navigation" class="menu">

        <a href="#" class="logotype">M<span>GOONGIE</span></a>

        <div class="overflow-container">

            <ul class="menu-dropdown">

                <li><a href="#">Dashboard</a><span class="icon"><i class="fa fa-dashboard"></i></span></li>
                <li class="range-price">
                    <form action="" class="price_range">
                        <label for="range">Range by price</label> <br><br>
                        <input type="text" id="range" name="range" value=""/>
                        <br><br>
                    </form>
                    <span class="icon"><i class="fa fa-dashboard"></i></span>
                </li>
                <li class="range-price">
                    <a href="{{ route('product.index', ['sort' => 'asc']) }}" class="btn btn-primary w-50">low to high</a>
                    <a href="{{ route('product.index', ['sort' => 'desc']) }}" class="btn btn-primary w-50">high to low</a>
                </li>
                <li><input type="reset" class="btn btn-primary w-50 reset_button" ></li>

                <li><a href="#">Favourites</a><span class="icon"><i class="fa fa-heart"></i></span></li>

                <li><a href="#">Messages</a><span class="icon"><i class="fa fa-envelope"></i></span></li>

            </ul>

        </div>

    </nav>

</div>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('product.index') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('categories.index') }}">Category</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('product.create') }}">Product</a>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav ml-auto">
                    <!-- Authentication Links -->

                    <li class="nav-item">
                        <input class="form-control js_live_search" type="text" placeholder="Search" aria-label="Search"
                               name="search_by_name">
                    </li>
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            @if (Route::has('register'))
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            @endif
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <main class="py-4">
        @yield('content')
    </main>
</div>


<script>
    var replyStoreRoute = '{{route('replies.store')}}'
</script>

<!-- Scripts -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{asset('js/index.js')}}"></script>
<script src="{{ asset('js/ion.rangeSlider.js') }}"></script>

</body>
</html>
