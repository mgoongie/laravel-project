{{--Component to be rendered  when range works--}}

<table class="table product_list" id="result_table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">name</th>
        <th scope="col">price</th>
        <th scope="col">created at</th>
        <th scope="col">edit</th>
        <th scope="col">delete</th>
    </tr>
    </thead>

    @foreach($results as $result)
        <tr data-id="{{$result->id}}">
            <td data-target="name">
                @if(count($result->files) != 0)
                    <img src="{{asset($result->files[0]->name)}}" alt=""
                         style="height: 70px; width: 70px">
                @else
                    <img src="https://cdn3.iconfinder.com/data/icons/faticons/32/picture-01-512.png"
                         alt="" style="height: 70px; width: 70px">
                @endif
            </td>
            <td data-target="price">{{$result->price}}</td>
            <td data-target="created_at">{{$result->created_at}}</td>
            <td>
                <button data-id="{{$result->id}}" type="button"
                        class="btn btn-secondary js_edit_product">Edit
                </button>
            </td>
            <td>
                <button data-id="{{$result->id}}" type="button"
                        class="btn btn-secondary js_delete_product">Delete
                </button>
            </td>
        </tr>
    @endforeach
</table>

