@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <form method="POST" action="{{ route('categories.store') }}" class="col-md-5 mx-auto mt-5" id="category_form">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>
                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name"  autofocus>
                        <span role="alert">
                    <strong class="errors"></strong>
                        </span>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-3">
                    <button type="button" class="btn btn-primary js_category_save">
                        {{ __('Save') }}
                    </button>
                </div>
            </div>
        </form>
        <div class="col-md-4">
            <table class="table table-sm" id="category_list">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Created at</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr data-id="{{$category->id}}">
                        <td data-id='category_id' scope="row" data-target='id'>{{$category->id}}</td>
                        <td data-target='name'>{{$category->name}}</td>
                        <td data-created="created_at" data-target='created_at'>{{$category->created_at}}</td>
                        <td>
                            <button data-id="{{$category->id}}" type="button" class="btn btn-secondary js_edit_category">
                                Edit
                            </button>
                        </td>
                        <td>
                            <button data-id="{{$category->id}}" type="submit" class="btn btn-secondary js_delete_category">
                                Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->

    <div class="modal fade" id="category_edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Make changes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="js_updated_data">
                        @csrf
                        <input type="hidden" name="id" value="">
                        <div class="form-group row">
                            <label for="category_name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="category_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="category_type" class="col-sm-2 col-form-label">Updated_at</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="created_at" id="category_type">
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary js_update_category">Update</button>
                </div>
            </div>
        </div>
    </div>
@endsection